#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from OpenSSL import crypto, SSL 
from os.path import basename, dirname, splitext
from sys import exit
from tkinter import Button, E, Entry, Label, LEFT, Tk, W, filedialog, messagebox, simpledialog


# Define password query dialog
class _QueryPasswordDialog(simpledialog._QueryDialog):
    def body(self, master):
        w = Label(master, text=self.prompt, justify=LEFT)
        w.grid(row=0, padx=5, sticky=W)

        self.entry = Entry(master, name="entry")
        self.entry.grid(row=1, padx=5, sticky=W+E)

        self.toggle = Button(master, text="⚷", command=self.toggleShow)
        self.toggle.grid(row=1, padx=5, column=1, sticky=W+E)

        if self.initialvalue is not None:
            self.entry.insert(0, self.initialvalue)
            self.entry.select_range(0, END)

        return self.entry

    def toggleShow(self):
        if self.entry["show"] == "":
            self.entry["show"] = "●"
        else:
            self.entry["show"] = ""

class _QueryPassword(_QueryPasswordDialog):
    def __init__(self, *args, **kw):
        if "show" in kw:
            self.__show = kw["show"]
            del kw["show"]
        else:
            self.__show = "●"
        _QueryPasswordDialog.__init__(self, *args, **kw)

    def body(self, master):
        entry = _QueryPasswordDialog.body(self, master)
        if self.__show is not None:
            entry.configure(show=self.__show)
        return entry

    def getresult(self):
        return self.entry.get()

def askpassword(title, prompt, **kw):
    d = _QueryPassword(title, prompt, **kw)
    return d.result


# Procedure to check whether the key matches the cert
def cert_key_match(certificate, key):
    try:
        context = SSL.Context(SSL.TLSv1_METHOD)
        context.use_certificate(certificate)
        context.use_privatekey(key)
        return True
    except SSL.Error:
        return False


# Create and hide root window
root = Tk()
root.withdraw()


# Read input file
while True:
    infile = filedialog.askopenfilename(title='PEM-Eingabedatei',
                                        filetypes=(('PEM-Dateien', ('*.pem', '*.PEM')),
                                                   ('alle Dateien', '*.*')))
    if not(infile):
        messagebox.showinfo('Abbruch', 'Die Dateiauswahl wurde abgebrochen!')
        exit(0)

    with open(infile, 'r') as file:
        pemstring = file.read()

    delim = 'deadbeef'
    while delim in pemstring:
        delim = delim + delim

    pemstrings = pemstring.replace('-----BEGIN ', delim + '-----BEGIN ').split(delim)
    pemcerts = [x for x in pemstrings if '-----BEGIN CERTIFICATE-----' in x]
    pemkeys = [x for x in pemstrings if ('-----BEGIN RSA PRIVATE KEY-----' in x) or ('-----BEGIN ENCRYPTED PRIVATE KEY-----' in x)]

    # Break the loop if we are happy, shout out otherwise
    if len(pemcerts) == 0:
        messagebox.showinfo('Fehler!', 'Kein X.509-Zertifikat in Datei {} gefunden!'.format(infile))
    elif len(pemkeys) == 0:
        messagebox.showinfo('Fehler!', 'Kein geheimer Schlüssel in Datei {} gefunden!'.format(infile))
    elif len(pemkeys) > 1:
        messagebox.showinfo('Fehler!', 'Mehrere geheime Schlüssel in Datei {} gefunden!'.format(infile))
    else:
        break

certificates = list(map(lambda x: crypto.load_certificate(crypto.FILETYPE_PEM, x), pemcerts))

# Read password
while True:
    password = askpassword('Passworteingabe', 'Passwort des geheimen Schlüssels:')
    if password is None:
        messagebox.showinfo('Abbruch', 'Die Passworteingabe wurde abgebrochen!')
        exit(0)

    # Break the loop if the password decrypts the private key, shout out
    # otherwise
    try:
        key = crypto.load_privatekey(crypto.FILETYPE_PEM, pemkeys[0], password.encode('utf-8'))
        certificate = [x for x in certificates if cert_key_match(x, key)]
        if len(certificate) == 0:
            messagebox.showinfo('Fehler!', 'Geheimer Schlüssel passt zu keinem Zertifikat!')
            exit(1)
        elif len(certificate) > 1:
            messagebox.showinfo('Fehler!', 'Geheimer Schlüssel passt zu mehreren Zertifikaten!')
            exit(2)
        else:
            break
    except crypto.Error:
        messagebox.showinfo('Fehler!', 'Geheimer Schlüssel konnte nicht dechiffriert werden!')

certificate = certificate[0]
ca_certificates = [x for x in certificates if certificate.digest('md5') != x.digest('md5')]


# Assemble P12 structure
p12 = crypto.PKCS12()
p12.set_ca_certificates(ca_certificates)
p12.set_certificate(certificate)
p12.set_privatekey(key)


# Read and write output file
while True:
    outfile = filedialog.asksaveasfilename(title='PKCS12-Ausgabedatei',
                                           initialdir=dirname(infile),
                                           initialfile=splitext(basename(infile))[0]+'.p12',
                                           defaultextension='.p12',
                                           filetypes=(('PKCS12-Dateien', ('*.p12', '*.P12', '*.pfx', '*.PFX')),
                                                      ('alle Dateien', '*.*')))
    if not(outfile):
        messagebox.showinfo('Abbruch', 'Die Dateiauswahl wurde abgebrochen!')
        exit(0)

    try:
        with open(outfile, 'wb') as file:
            file.write(p12.export(password.encode('utf-8')))
        break
    except:
        messagebox.showinfo('Fehler!', 'Kann PKCS12-Datei {} nicht schreiben!'.format(outfile))


# All done
messagebox.showinfo('Zertifikat exportiert!', 'Zertifikat und Schlüssel erfolgreich in PKCS12-Datei {} exportiert!'.format(outfile))
