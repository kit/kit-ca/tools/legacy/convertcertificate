ConvertCertificate
==================

This is a minimal Python script designed to convert an X.509
certificate/key file in PEM format into PKCS12 format suitable for import
into the Windows certificate store.

The script expects a PEM file with a certificate and the corresponding private
key.  Any additional certificates contained in the file are assumed to be the CA
chain for the certificate.

Windows Binary
==============

A 32-bit stand-alone Windows binary with the script is provided in [the
release notes](https://git.scc.kit.edu/KIT-CA/ConvertCertificate/tags).  To
use the Windows binary, simply double-click the executable file.

If you want to roll the Windows EXE file yourself, you will need a Windows
system with a working Python 3 installation and the `pyinstaller` and the
`pyopenssl` packages installed.  Running `Windows\roll_executable.bat` should
create a working binary in `dist\RecodeCertificate.exe`.
